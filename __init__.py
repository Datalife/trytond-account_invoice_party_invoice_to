# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .party import Party, PartyInvoiceTo


def register():
    Pool.register(
        Party,
        PartyInvoiceTo,
        module='account_invoice_party_invoice_to', type_='model')
